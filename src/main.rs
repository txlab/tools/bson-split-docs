use clap::Parser;
use ctrlc;
use std::fs::{self};
use std::time::Instant;
use unsplit::unsplit;

use anyhow::Context;
use cli::{CliOpts, Commands};

use crate::split::split_file;
use crate::utils::{ensure_empty_dir, print_stats};

mod cli;
mod split;
mod unsplit;
mod utils;

fn main() -> anyhow::Result<()> {
    ctrlc::set_handler(|| {
        std::process::exit(128 + 2 /* SIGINT */);
    })
    .expect("Error setting Ctrl-C handler");

    let cli = CliOpts::parse();
    // validate(&opt);

    match &cli.command {
        Commands::Split {
            json,
            input,
            target,
            delete,
        } => {
            let input_meta = fs::metadata(&input)
                .with_context(|| format!("Failed to read input: {}", input.display()))?;
            if input_meta.is_dir() {
                ensure_empty_dir(&target, delete)?;
                let mut total: i64 = 0;
                let start = Instant::now();
                let dir_list = fs::read_dir(&input)
                    .with_context(|| format!("Failed to list directory: {}", input.display()))?;
                for file in dir_list {
                    let file = file
                        .with_context(|| format!("Failed to list file in: {}", &input.display()))?;
                    let path = file.path();
                    // println!("file: {:?} {:?}", file, path.extension());
                    if !path
                        .extension()
                        .is_some_and(|ext| ext.to_str().is_some_and(|ext| ext == "bson"))
                    {
                        continue;
                    }
                    let collection = path
                        .file_stem()
                        .expect(format!("Failed to extract filename from {:?}", file).as_str());
                    // println!("bson: {:?} -> {:?}", file, collection.to_str());

                    total += split_file(&path, &target.join(collection), json, &cli.stats)?;
                }

                println!("\nDone. Total stats:");
                print_stats(start, total, true)?;
            } else {
                split_file(&input, &target, json, &cli.stats)?;
                println!("\nDone.");
            }
        }

        Commands::Unsplit {
            input,
            target,
            delete,
        } => {
            unsplit(input, target, &cli.stats, delete)?;
        }
    };

    Ok(())
}
