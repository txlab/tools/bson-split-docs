use crate::{
    split::unsplit_file,
    utils::{ensure_empty_dir, file_name, file_stem, first_file_entry, is_hidden, print_stats},
};
use anyhow::{bail, Context, Result};
use inline_colorization::*;
use std::{
    fs::{self, File},
    path::PathBuf,
    time::Instant,
};

pub fn unsplit(input_dir: &PathBuf, target: &PathBuf, stats: &i64, delete: &bool) -> Result<()> {
    let mut total_count: i64 = 0;
    let start = Instant::now();

    // Read single file to detect if a collection folder or a db folder (with collection folders)
    let entry = first_file_entry(&input_dir)?;
    match entry {
        Some(dir_entry) => {
            if dir_entry.path().is_dir() {
                ensure_empty_dir(&target, delete)?;
                total_count += unsplit_db(&input_dir, target, stats)?;
            } else {
                total_count += unsplit_collection(&input_dir, target, stats)?;
            }
        }
        None => {
            eprintln!("{color_yellow}Error: empty input folder - doing nothing{color_reset}");
        }
    }

    println!("\nDone. Total stats:");
    print_stats(start, total_count, true)?;

    Ok(())
}

fn unsplit_db(input: &PathBuf, target: &PathBuf, stats: &i64) -> Result<i64> {
    let mut count: i64 = 0;
    let dir_list = fs::read_dir(&input)
        .with_context(|| format!("Failed to list directory: {}", input.display()))?;

    for file in dir_list {
        let path = file?.path();
        // Skip if it's a hidden file or directory
        if is_hidden(&path)? {
            continue;
        }
        if !path.is_dir() {
            bail!(
                "Nested dir inside collection '{}': {}",
                file_name(&path)?,
                path.display()
            )
        }
        println!("Collection dir: {}", path.display());
        count += unsplit_collection(&path, target, stats)?;
    }
    Ok(count)
}

fn unsplit_collection(input: &PathBuf, target_dir: &PathBuf, stats: &i64) -> Result<i64> {
    let mut count: i64 = 0;
    let start = Instant::now();

    let collection = file_name(input)?;
    let dir_list = fs::read_dir(&input)
        .with_context(|| format!("Failed to list directory: {}", input.display()))?;
    let target_path = target_dir.join(collection + ".bson");
    let target_file = File::create(&target_path)
        .with_context(|| format!("Failed to create target file: {}", &target_path.display()))?;
    let mut target_buffer = std::io::BufWriter::new(target_file);

    for file in dir_list {
        let file = file.with_context(|| format!("Failed to list file in: {}", &input.display()))?;
        let path = file.path();
        // println!("file: {:?} {:?}", file, path.extension());

        if path.is_dir() {
            eprintln!("{color_yellow}Warn: directory inside collection folder (expecting files only), skipping: {}{color_reset}", input.display());
            continue;
        }

        match path.extension().and_then(|ext| ext.to_str()) {
            Some(ext @ "bson") | Some(ext @ "json") => {
                unsplit_file(&path, &mut target_buffer, ext == "json", stats)?;
            }
            _ => continue,
        }

        // println!("bson: {:?} -> {:?}", file, collection.to_str());
        count += 1;
        if *stats > 0 && count % stats == 0 {
            print_stats(start, count, false)?;
        }
    }
    print_stats(start, count, true)?;
    Ok(count)
}
