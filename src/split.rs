use std::fs::{self, File};
use std::io::{BufReader, BufWriter, Write};
use std::path::PathBuf;
use std::time::Instant;

use anyhow::{Context, Result};

use bson::Document;

use crate::utils::{create_file, print_stats};

pub fn split_file(
    input: &PathBuf,
    target: &PathBuf,
    json: &bool,
    stats: &i64,
) -> anyhow::Result<i64> {
    let start = Instant::now();
    // println!("Reading {}", input.display());
    let f = File::open(&input).with_context(|| "Failed to open file")?;
    let mut buffer_in = BufReader::new(f);

    let mut num: i64 = 0;
    let mut writes: i64 = 0;

    let path = &target;
    println!("Splitting {} to {}", input.display(), path.display());
    fs::create_dir_all(path)?;

    while let Some(doc) = process_doc(Document::from_reader(&mut buffer_in))? {
        // print!("{:?}", doc);
        // println!("{:?}", doc.get("_id"));
        let id = if let Some(id_field) = doc.get("_id") {
            if let Some(s) = id_field.as_str() {
                anyhow::Ok(s.to_string())
            } else {
                Ok(id_field
                    .as_object_id()
                    .expect(format!("Neither string nor ObjectId as _id: {:?}", id_field).as_str())
                    .to_hex())
            } //.with_context(|| format!("Invalid _id field: {:?}", id_field))
        } else {
            num += 1;
            Ok(num.to_string())
        }?;
        // println!("{:?} => {}", doc.get("_id"), id);

        let mut writer = create_file(path, id).with_context(|| "Failed to create file")?;
        if *json {
            let serialized = serde_json::to_string_pretty(&doc)?;
            writer.write_all(serialized.as_bytes())?;
        } else {
            doc.to_writer(&mut writer)
                .with_context(|| "Failed to write document")?;
        }
        // writer.flush()?; //? needed?
        writes += 1;
        if *stats > 0 && writes % stats == 0 {
            print_stats(start, writes, false)?;
        }
    }
    print_stats(start, writes, true)?;
    Ok(writes)
}

pub fn unsplit_file(
    input: &PathBuf,
    target_buffer: &mut dyn Write,
    json: bool,
    stats: &i64,
) -> Result<()> {
    let start = Instant::now();

    let f =
        File::open(input).with_context(|| format!("Failed to open file {}", input.display()))?;
    let mut buffer_in = BufReader::new(f);

    let doc = if json {
        let json_str = std::fs::read_to_string(input)
            .with_context(|| format!("Failed to read JSON file {}", input.display()))?;
        serde_json::from_str::<Document>(&json_str).with_context(|| "Failed to parse JSON file")?
    } else {
        Document::from_reader(&mut buffer_in)
            .with_context(|| "Failed to read document from BSON file")?
    };

    doc.to_writer(target_buffer)
        .with_context(|| "Failed to write document to target file")?;

    Ok(())
}

fn process_doc(doc: bson::de::Result<Document>) -> anyhow::Result<Option<Document>> {
    match doc {
        Ok(doc) => Ok(Some(doc)),
        Err(e) => match e {
            bson::de::Error::IoError(e) => {
                // println!("{:#?}", e);
                if e.kind() == std::io::ErrorKind::UnexpectedEof
                /* && e.message() == "failed to fill whole buffer" */
                {
                    Ok(None)
                } else {
                    Err(e).with_context(|| "IO error")
                }
            }
            _ => Err(e).with_context(|| "BSON error"),
        },
    }
}
