use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None, disable_help_flag(true))]
pub struct CliOpts {
    #[command(subcommand)]
    pub command: Commands,

    /// Output only first 10 docs (for testing)
    #[clap(short, long, action)]
    pub head: bool,
    /// Print stats every X documents (0 to disable)
    #[clap(short, long, default_value_t = 1000)]
    pub stats: i64,
}

#[derive(Subcommand, Debug)]
pub enum Commands {
    /// Initialize a svc
    Split {
        /// The path to the file to read
        #[clap(value_parser)]
        input: std::path::PathBuf,
        /// The target directory
        #[clap(value_parser)]
        target: std::path::PathBuf,

        /// Output as pretty JSON instead of BSON
        #[clap(short, long, action)]
        json: bool,
        /// Delete target contents first
        #[clap(short, long, action)]
        delete: bool,
    },
    Unsplit {
        /// The directory to read
        #[clap(value_parser)]
        input: std::path::PathBuf,
        /// The target directory
        #[clap(value_parser)]
        target: std::path::PathBuf,
        /// Delete target contents first
        #[clap(short, long, action)]
        delete: bool,
    },
}

// pub fn validate(opt: &CliOpts) {
// }
