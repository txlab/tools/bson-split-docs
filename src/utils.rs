use std::fs::{self, DirEntry, File};
use std::io::{BufWriter, Write};
use std::path::PathBuf;
use std::time::Instant;

use anyhow::{bail, Context, Result};

pub fn create_file(path: &PathBuf, name: String) -> Result<BufWriter<File>> {
    let filename = path.join(format!("{}.json", name));
    let file = File::create(filename)?;
    let writer = BufWriter::new(file);
    Ok(writer)
}

pub fn print_stats(start: Instant, count: i64, linefeed: bool) -> Result<()> {
    print!(
        "\r{} in {:.1}s = {:.0}/s",
        count,
        start.elapsed().as_secs_f32(),
        count as f32 / start.elapsed().as_secs_f32()
    );
    if linefeed {
        println!();
    } else {
        std::io::stdout().flush()?;
    }
    Ok(())
}
pub fn file_stem(path: &PathBuf) -> Result<String> {
    Ok(path
        .file_stem()
        .with_context(|| format!("Failed to extract filename from {:?}", path))?
        .to_str()
        .with_context(|| format!("unicode convert filename '{:?}'", path))?
        .to_owned())
}
pub fn file_name(path: &PathBuf) -> Result<String> {
    Ok(path
        .file_name()
        .with_context(|| format!("Failed to extract filename from {:?}", path))?
        .to_str()
        .with_context(|| format!("unicode convert filename '{:?}'", path))?
        .to_owned())
}
pub fn is_hidden(path: &PathBuf) -> Result<bool> {
    Ok(path
        .file_name()
        .context("get filename")?
        .to_str()
        .context("utf8 filename")?
        .starts_with('.'))
}
pub fn first_file_entry(path: &PathBuf) -> Result<Option<DirEntry>> {
    let mut entries =
        fs::read_dir(path).with_context(|| format!("List input dir: {}", path.display()))?;
    Ok(match entries.next() {
        Some(res_entry) => Some(res_entry.context("Failed to get directory entry")?),
        None => None,
    })
}
pub fn ensure_empty_dir(dir: &PathBuf, should_delete: &bool) -> Result<()> {
    if dir.exists() {
        if *should_delete {
            println!("Deleting contents of {}", dir.display());
            fs::remove_dir_all(dir)?;
            fs::create_dir(dir)?;
        } else if first_file_entry(dir)?.is_some() {
            bail!("Directory not empty: {}", dir.display());
        }
    } else {
        fs::create_dir_all(dir)?;
    }
    Ok(())
}
