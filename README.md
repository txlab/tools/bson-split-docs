# bson-split-docs

Splits a [BSON](http://bsonspec.org/) file (or multiple) into separate files per document, optionally in JSON format.


### Installation

Install using Cargo:

```bash
#TODO: cargo install bson-split-docs
```
So...
- Install [rust tools](https://rustup.rs/)
- ... or install [devshell](https://numtide.github.io/devshell/intro.html) (will setup everything you need, without installing it on your machine 😜)
- Clone, build & link (or copy):
```bash
git clone https://gitlab.com/txlab/tools/bson-split-docs.git
cd bson-split-docs
cargo build --release
ln -s $PWD/target/release/bson-split-docs ~/.local/bin/bson-split-docs # copy if you don't plan to keep the repo around
```
- run & watch via task:
```bash
task dev -- unsplit -j tmp/split/ tmp/import/
```

## Usage

### Split
```bash
# Single file -> directory with output files
bson-split-docs split -j \
  ~/dev/dump/mongo/db-name/collection.bson \
  db-name/collection/
# or directory with many .bson files (output will be in subdirectories same as bson file name)
bson-split-docs split -j ~/dev/dump/mongo/db-name/ db-name/
```

### Re-assemble
```bash
# directories with json files -> Single bson file per collection
bson-split-docs unsplit -j \
  json-data/db-name/collection/ \
  mongo-import/db-name/collection.bson
# or: directories per collection with json files -> Single bson file per collection
bson-split-docs unsplit --delete ~/dev/dump/mongo/db-name/ import/db-name/
```